import _ from 'lodash'
import { TITLE } from '@/config'

export const toText = (html) => {
  return html ? String(html).replace(/<[^>]+>/gm, '') : ''
}

export const toTitle = (html) => {
  let title = toText(html)
  let pageTitle = []
  if (!_.isEmpty(title)) {
    pageTitle.push(title)
  }
  pageTitle.push(TITLE)

  return pageTitle.join(' | ')
}

export const slugify = (string) => {
  return _.deburr(string).trim().toLowerCase().replace(/ /g, '-').replace(/([^a-zA-Z0-9._-]+)/, '')
}

export const cookie = {
  set (name, value, days = 365) {
    let expires
    if (days) {
      let date = new Date()
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
      expires = '; expires=' + date.toGMTString()
    } else {
      expires = ''
    }
    document.cookie = name + '=' + value + expires + '; domain=' + window.location.hostname + '; path=/'
    // document.cookie = name + '=' + value + expires + '; domain=' + tldjs.getDomain(window.location.hostname) + '; path=/'
  },
  get (name) {
    const nameEQ = name + '='
    const ca = document.cookie.split(';')
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i]
      while (c.charAt(0) === ' ') {
        c = c.substring(1, c.length)
      }
      if (c.indexOf(nameEQ) === 0) {
        return c.substring(nameEQ.length, c.length)
      }
    }
    return null
  },
  del (name) {
    this.set(name, '', -1)
  }
}

export const flattenTree = (tree) => {
  function recurse (nodes, path) {
    return _.map(nodes, function (node, index) {
      let newPath = [...path, index]
      let result = [_.assign({ path: newPath.join('.'), level: path.length }, _.omit(node, 'children'))]
      if (node && _.get(node, 'children.0')) {
        result.push(recurse(node.children, [...newPath, 'children']))
      }
      return result
    })
  }
  return _.flattenDeep(recurse(tree, []))
}
