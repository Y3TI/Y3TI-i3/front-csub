// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import _ from 'lodash'

import App from './App'
import router from './router'
import store from './store'
import config from './config'

import VTooltip from 'v-tooltip'

import InlineEditing, { i3StoreModules } from 'y3ti-i3'

import Routing, { r3StoreModules } from 'y3ti-r3'
// const r3StoreModules = {}

// import Page from '@/components/Page'
// import DefaultLayout from '@/components/Layouts/DefaultLayout'

// console.log('InlineEditing', InlineEditing)

Vue.use(Routing, {
  store,
  router,
  config
})

Vue.use(InlineEditing, {
  store,
  router,
  config
})

_.each(_.merge({}, i3StoreModules, r3StoreModules), (module, name) => {
  store.registerModule(name, module)
})

console.log('--router', router)

Vue.use(VTooltip)
// import vOutsideEvents from 'vue-outside-events'
// Vue.use(vOutsideEvents)

// import EditableLink from '@/components/Editable/Fields/Inline/Composite/LinkInlineField'
// import EditableTextfield from '@/components/Editable/Fields/Inline/TextFieldInlineField'
// import EditableImage from '@/components/Editable/Fields/Inline/Composite/ImageInlineField'
// import EditableZone from '@/components/Editable/Utils/ZoneEditable'
// Vue.component('reuse-media-checkbox', () => import('@/components/Editable-Custom/ReuseMediaCheckbox'))

// Vue.component('editable-link', () => import('@/components/Editable/Fields/Inline/Composite/LinkInlineField'))
// Vue.component('editable-textfield', () => import('@/components/Editable/Fields/Inline/TextFieldInlineField'))
// Vue.component('editable-image', () => import('@/components/Editable/Fields/Inline/Composite/ImageInlineField'))
// Vue.component('editable-zone', () => import('@/components/Editable/Utils/ZoneEditable'))

Vue.config.productionTip = false

// dynamicRouter.addRoutes([
//   {
//     path: '',
//     component: DefaultLayout,
//     children: [{
//       path: '',
//       components: {
//         // 'menu': MenuBlock,
//         'main-content': { template: '<router-view/>' }
//       },
//       children: [{
//         path: '/welcom-yfgnvbe',
//         name: 'Page',
//         props: (route) => ({ path: route.path }),
//         component: Page
//       }]
//     }]
//   }
//   // {
//   //   path: '/welcom-yfgnvbe',
//   //   name: 'Page',
//   //   props: (route) => ({ path: route.path }),
//   //   component: Page
//   // }
// ])

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
