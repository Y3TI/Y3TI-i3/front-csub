import _ from 'lodash'
import axios from 'axios'

import { API_URL, QUERY_MERGE } from '@/config'

// import auth from '@/auth'

let instance = axios.create({
  baseURL: API_URL,
  headers: { 'Content-Type': 'application/json' }
  // withCredentials: true
})

const requestWrapper = (method, baseURL) => {
  return (endpoint, options) => {
    options = options || {}
    // options.auth = options.auth || options.auth === undefined
    options.data = options.body || {}
    options.headers = options.headers || {}
    // options.headers = _.mapKeys(options.headers, (val, key) => _.startsWith('x-', key) ? key : 'x-' + key)

    options.query = _.merge({}, options.query, QUERY_MERGE)

    const reqSettings = {
      url: endpointFormatter(endpoint, options.params, options.query),
      method,
      data: options.data,
      headers: options.headers
    }

    if (baseURL) {
      reqSettings.baseURL = baseURL
    }

    // if (options.auth && auth) {
    //   return auth.handler(instance, reqSettings).catch((err) => {
    //     if (process.env.NODE_ENV === 'development') {
    //       console.log(err)
    //     }
    //     return Promise.reject(err.response || err)
    //   })
    // } else {
    return instance.request(reqSettings).catch((err) => {
      if (process.env.NODE_ENV === 'development') {
        console.log(err)
      }
      return Promise.reject(err.response || err)
    })
    // }
  }
}

const endpointFormatter = (path, params, query) => {
  params = params || {}
  query = query || {}

  let replacedPath = path
  _.each(params, (variable, key) => {
    replacedPath = replacedPath.replace(new RegExp(':' + key, 'g'), variable)
  })

  let getParams = ''
  if (!_.isEmpty(query) && _.isPlainObject(query)) {
    getParams = '?' + _.keys(query).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(query[k])}`).join('&')
  }

  return replacedPath + getParams
}

const methodList = ['delete', 'get', 'head', 'post', 'put', 'patch']

let req = (baseURL) => {
  let resp = {}
  _.each(methodList, (method) => {
    resp[method] = requestWrapper(method, baseURL)
  })
  return resp
}
_.each(methodList, (method) => {
  req[method] = requestWrapper(method)
})

export const request = req
