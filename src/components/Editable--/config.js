import _ from 'lodash'

import TextFieldWidget from '@/components/Editable/Fields/Widgets/TextFieldWidgetField'
import LinkWidget from '@/components/Editable/Fields/Widgets/LinkWidgetField'
import TextAreaWidget from '@/components/Editable/Fields/Widgets/TextAreaWidgetField'
import ImageWidget from '@/components/Editable/Fields/Widgets/ImageWidgetField'
import SettingsWidget from '@/components/Editable/Fields/Widgets/SettingsWidgetField'
import DefaultWidget from '@/components/Editable/Fields/Widgets/DefaultFieldWidgetField'

import DropdownSettings from '@/components/Editable/Fields/Settings/DropdownSettingsField'
import CheckboxSettings from '@/components/Editable/Fields/Settings/CheckboxSettingsField'
import RadioSettings from '@/components/Editable/Fields/Settings/RadioSettingsField'

const conf = require('@/config')

export const EDITABLE_FIELDS_GROUP_CONFIG = (props) => {
  // const { config = {}, node, path = [], data } = props
  let groupOptions = {
    // beforeAll: [ 'reuse-media-checkbox' ],
    // afterAll: [ 'reuse-media-checkbox' ]
  }
  // console.log('config', config)
  // if (config.id === 'accordion') {
  //   groupOptions.hideAll = true
  // }
  // console.log('conf', conf)
  return _.merge({}, groupOptions, conf.EDITABLE_FIELDS_GROUP_CONFIG(props))
}

export const EDITABLE_FIELD_CONFIG = (props) => {
  const { parentConfig = {}, config = {}, parentData } = props
  // const { parentConfig = {}, config = {}, node, path = [], data, parentData } = props
  // console.log(ReuseMediaCheckbox)
  let fieldOptions = {
    // beforeField: {},
    // afterField: {}
  }

  if (parentConfig.reference === 'media' && config.type === 'image') {
    fieldOptions = {
      beforeField: [ 'reuse-media-checkbox' ]
      // hideField: true
      // afterField: [ 'reuse-media-checkbox' ]
    }
  }
  // if (config.path === 'page.rows[].title') {
  //   console.log('parentData', parentData)
  //   console.log('parentConfig', parentConfig)
  // }

  if (config.path === 'page.rows[].title' && _.get(parentData, ['settings', 'position']) === 'left') {
    fieldOptions.hideField = true
  }

  // console.log('conf', conf)
  return _.merge({}, fieldOptions, conf.EDITABLE_FIELD_CONFIG(props))
}

const defaultWidgetsMapping = {
  'link_default': LinkWidget,
  'string_textfield': TextFieldWidget,
  'field_text_formatted': TextFieldWidget,
  'text_with_summary': TextAreaWidget,
  'string_textarea': TextFieldWidget,
  'text_textarea': TextAreaWidget,
  'image_image': ImageWidget
}

export const EDITABLE_WIDGETS_MAPPING = (props) => {
  // const { parentConfig = {}, config = {}, node, path = [], data, parentData } = props
  const { config = {} } = props

  const customConfig = conf.EDITABLE_WIDGETS_MAPPING(props)
  if (customConfig) {
    return customConfig
  }

  if (config.id === 'field_settings') {
    return SettingsWidget
  }

  for (let display of config.display || []) {
    if (defaultWidgetsMapping[display]) {
      return defaultWidgetsMapping[display]
    }
  }

  return DefaultWidget
}

export const EDITABLE_SETTINGS_FIELD_CONFIG = (props) => {
  const { key, data } = props
  // const { config = {}, fieldConfig = {}, node, path = [], key, data, fieldData } = props
  // export const EDITABLE_SETTINGS_FIELD_CONFIG = (config = {}, fieldConfig = {}, editedNode, currentPath = []) => {
  // console.log(ReuseMediaCheckbox)
  let fieldOptions = {
    // beforeField: {},
    // afterField: {}
  }

  // console.log(data)
  if (data && data.position === 'left' && key === 'template') {
    fieldOptions.hideField = true
  }

  // if (config.reference === 'media' && fieldConfig.type === 'image') {
  //   fieldOptions = {
  //     beforeField: [ 'reuse-media-checkbox' ]
  //     // hideField: true
  //     // afterField: [ 'reuse-media-checkbox' ]
  //   }
  // }
  // console.log('conf', conf)
  return _.merge({}, fieldOptions, conf.EDITABLE_SETTINGS_FIELD_CONFIG(props))
}

const defaultSidebarFieldsMapping = {
  'dropdown': DropdownSettings,
  'checkbox': CheckboxSettings,
  'radio': RadioSettings
}

export const EDITABLE_SETTINGS_FIELDS_MAPPING = (props) => {
  const { fieldConfig } = props

  const customConfig = conf.EDITABLE_SETTINGS_FIELDS_MAPPING(props)
  if (customConfig) {
    return customConfig
  }

  if (defaultSidebarFieldsMapping[fieldConfig.type]) {
    return defaultSidebarFieldsMapping[fieldConfig.type]
  }

  // return DefaultSettings
}

export const EDITABLE_NODE_PRESETS = conf.EDITABLE_NODE_PRESETS
// export const EDITABLE_NODE_PRESETS = (node) => conf.EDITABLE_NODE_PRESETS(node)
export const EDITABLE_GHOST_ZONES = (node) => conf.EDITABLE_GHOST_ZONES(node)
