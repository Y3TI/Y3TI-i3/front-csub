import Vue from 'vue'
import Vuex from 'vuex'

// import Y3TI from './modules/Y3TI'
// import page from './modules/page'

// import InlineEditor from '@ckeditor/ckeditor5-build-inline'
// import VueCkeditor from 'vue-ckeditor5'

// Vue.use(VueCkeditor.plugin, {
//   editors: {
//     inline: InlineEditor
//   },
//   name: 'ckeditor'
// })

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    // Y3TI
    // page
  },
  strict: debug
})
