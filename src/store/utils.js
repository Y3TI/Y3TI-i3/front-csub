import _ from 'lodash'

export const mapSetAction = (actionParams) => {
  const { types, context, mutation, name, status, call, params = [], options = {} } = actionParams
  // params = params || []
  // options = options || {}
  if ((name !== false && (!_.isArray(name) || _.isEmpty(name))) || (status !== false && (!_.isArray(status) || _.isEmpty(status)))) {
    console.error(new Error('name and status should exists and be false or a not empty array'))
    return false
  }

  const { commit, state } = context
  if (options.lazy && !_.isEmpty(name)) {
    const existingValue = _.get(state, name)
    if (!_.isEmpty(existingValue)) {
      return existingValue
    }
  }

  if (status) {
    const moduleName = mutation.split('_')[0]
    const resetMutation = types[moduleName + '_RESET_STATUS']
    if (_.isFunction(options.commitReset)) {
      options.commitReset(context, resetMutation, actionParams)
    } else {
      setTimeout(() => {
        commit((moduleName === 'Y3TI' ? moduleName : moduleName.toLowerCase()) + '/' + resetMutation, actionParams, { root: true })
      }, 10 * 1000)
    }
  }

  const requestMutation = types[mutation + '_REQUEST']
  if (_.isFunction(options.commitRequest)) {
    options.commitRequest(context, requestMutation, actionParams)
  } else {
    commit(requestMutation, actionParams)
  }

  if (!_.isFunction(call)) {
    console.error(new Error('Unknown API function'))
    return false
  }

  return call(...params)
    .then((response) => {
      const successMutation = types[mutation + '_SUCCESS']
      if (_.isFunction(options.commitSuccess)) {
        options.commitSuccess(context, successMutation, { ...actionParams, response, data: response.data })
      } else {
        commit(successMutation, { ...actionParams, response, data: response.data })
      }
      return response
    })
    .catch((response) => {
      const failureMutation = types[mutation + '_FAILURE']
      if (_.isFunction(options.commitFailure)) {
        options.commitFailure(context, failureMutation, { ...actionParams, response, data: response.data })
      } else {
        commit(failureMutation, { ...actionParams, response, data: response.data })
      }
      return Promise.reject(response)
    })
}

export const mapSetMutations = ({ types, mutation, options }) => {
  options = options || {}

  const requestMutation = types[mutation + '_REQUEST']
  const successMutation = types[mutation + '_SUCCESS']
  const failureMutation = types[mutation + '_FAILURE']

  let mutations = {
    ...mutationSetStatus(requestMutation, 'loading'),
    ...mutationSetStatus(successMutation, 'success'),
    ...mutationSetStatus(failureMutation, 'fail')
  }

  if (_.isFunction(options.mutationRequest)) {
    mutations[requestMutation] = options.mutationRequest
  }

  if (_.isFunction(options.mutationSuccess)) {
    mutations[successMutation] = options.mutationSuccess
  }

  if (_.isFunction(options.mutationFailure)) {
    mutations[failureMutation] = options.mutationFailure
  }

  return mutations
}

export const mutationSetStatus = (mutation, value) => {
  return {
    [mutation] (state, { status }) {
      if (status) {
        // state[status[0]] = status.length > 1 ? _.setWith(_.clone(state[status[0]]), status.slice(1), value, _.clone) : value
        state[status[0]] = status.length > 1 ? setIn(state[status[0]], status.slice(1), value) : value
      }
    }
  }
}

export const mutationSave = (mutation) => {
  return {
    [mutation] (state, { name, data }) {
      if (name) {
        // state[name[0]] = name.length > 1 ? _.setWith(_.clone(state[name[0]]), name.slice(1), data, _.clone) : data
        state[name[0]] = name.length > 1 ? setIn(state[name[0]], name.slice(1), data) : data
      }
    }
  }
}

const __assign = (this && this.__assign) || Object.assign || function (t) {
  for (var s, i = 1, n = arguments.length; i < n; i++) {
    s = arguments[i]
    for (var p in s) {
      if (Object.prototype.hasOwnProperty.call(s, p)) {
        t[p] = s[p]
      }
    }
  }
  return t
}

const setIn = function (state, path, valueToSet) {
  if (_.isEmpty(path)) {
    return valueToSet
  }
  return _.setWith(__assign({}, state), path, valueToSet, function (nsValue, key, nsObject) {
    var nextKey = path[path.lastIndexOf(key) + 1]
    var isStringNumber = _.isString(nextKey) && _.isNumber(parseInt(nextKey, 10))
    var result = isStringNumber ? Object(nsValue) : nsValue
    return _.clone(result)
  })
}
