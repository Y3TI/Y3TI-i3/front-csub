// import _ from 'lodash'

// import * as types from '../mutation-types'
// import { mapSetAction, mapSetMutations, mutationSetStatus, mutationSave } from '../utils'
// import api from '@/api/Y3TI'

// import router from '@/router'

// const rawFormatterFrom = {
//   'inner-html-w-br': (input) => {
//     // console.log('input', input)
//     if (input === '<p>&nbsp;</p>') {
//       return null
//     }
//     let output = input
//     output = output.replace(/^<p>/g, '')
//     output = output.replace(/<\/p>$/g, '')
//     output = output.replace(/<\/p><p>/g, '<br/>')
//     output = output.replace(/&nbsp;/g, ' ')
//     // console.log('output', output)
//     return output
//   },
//   'inner-html': (input) => {
//     if (input === '<p>&nbsp;</p>') {
//       return null
//     }
//     let output = input
//     output = output.replace(/^<p>/g, '')
//     output = output.replace(/<\/p>$/g, '')
//     output = output === '' ? null : output
//     return output
//   }
// }

// const state = {
//   menu: {},
//   _menu: null,
//   editMode: true,
//   activeZone: {},
//   defaultActiveFields: [],
//   lastActiveFields: [],
//   defaultActiveTitle: null,
//   lastActiveTitle: null,
//   fieldsConfig: {},
//   _fieldsConfig: {},
//   // currentContentType: null
//   edited: {},
//   editableNodes: {},
//   originalNodes: {},
//   editedFields: {},
//   editedRawFields: {},

//   fileUpload: {},
//   _fileUpload: {},

//   sidebarNavigation: [],

//   altPressed: false,
//   originalNodesDisplayed: false,
//   newNodeId: Date.now(),

//   _contentTypes: null,
//   contentTypes: {},

//   nodesList: {},
//   _nodesList: {},
//   _nodesEdit: {},
//   _nodesAdd: {},

//   _routing: null,
//   routing: {},

//   _user: null,
//   user: {}
// }

// const getters = {
//   menu: state => state.menu,
//   menuStatus: state => state._menu,
//   editMode: state => state.editMode,
//   editableActiveZone: (state) => { return _.findKey(state.activeZone, (active) => active) },
//   getFileUploadStatus: (state) => (id) => { return _.get(state, ['_fileUpload', id]) },
//   // getFieldConfigStatus: (state) => (path) => {
//   //   return _.get(state, ['_fieldsConfig', ...path])
//   // },
//   getFieldConfig: (state) => (what) => {
//     if (_.isArray(what)) {
//       return _.get(state, ['fieldsConfig', ...what], {})
//     } else if (_.isPlainObject(what)) {
//       const { node, path } = what
//       // console.log('noooode', node)
//       // console.log('path', path)
//       // let partPath = [ this.node._type, 'nid-' + this.node.nid ]
//       let partPath = []
//       _.each(path, (pathItem, index) => {
//         let parentNode = false
//         if (index) {
//           const parentPath = path.slice(0, index)
//           parentNode = _.get(node, parentPath)
//         }

//         if (_.isString(pathItem)) {
//           // if (pathItem === '_type') {
//           // }
//           if (parentNode && parentNode._type) {
//             partPath.push('ref', parentNode._type)
//           }
//           partPath.push('fields', pathItem)
//         }
//       })
//       // console.log('paaaath', path)
//       // console.log('partPath', partPath)
//       return _.get(state, ['fieldsConfig', node._type, ...partPath], {})
//     }
//     return {}
//   },
//   // currentFieldsConfig: (state, getters) => { return _.get(state, ['fieldsConfig', getters.currentContentType], {}) },

//   getOriginalField: (state, getters) => ({ node, path }) => {
//     // const config = getters.getFieldConfig({ node, path })
//     let partPath = [ node._type, 'nid-' + node.nid ]

//     return _.get(state, ['originalNodes', ...partPath, ...path])
//     // const data = _.get(state, ['originalNodes', ...partPath, ...path], null)
//     // const output = config.cardinality === -1 ? config.default[0] : (config.cardinality === 1 ? config.default : (_.isArray(data) ? config.default[data.length] : null))

//     // return _.isNull(data) ? output : data
//   },

//   getEditedField: (state, getters) => ({ node, path = [], raw }) => {
//     let partPath = [ node._type, 'nid-' + node.nid ]

//     if (getters.isOriginalNodesDisplayed) {
//       return _.get(state, ['originalNodes', ...partPath, ...path], null)
//     }

//     if (raw) {
//       const data = _.get(state, ['editedRawFields', [...partPath, ...path, raw].join('.')])
//       if (data) { return data }
//     }
//     return _.get(state, ['edited', ...partPath, ...path])
//     // const data = _.get(state, ['edited', ...partPath, ...path], null)
//     // const config = getters.getFieldConfig({ node, path })
//     // const output = config.cardinality === -1 ? config.default[0] : (config.cardinality === 1 ? config.default : (_.isArray(data) ? config.default[data.length] : null))

//     // // console.log('data', data)
//     // // console.log('output', output)
//     // return _.isNull(data) ? output : data
//   },
//   getEditedNodes: (state, getters) => (path) => {
//     if (getters.isOriginalNodesDisplayed) {
//       return _.get(state, ['originalNodes', ...path])
//     }

//     return _.get(state, ['edited', ...path])
//   },
//   getEditableNodes: (state) => (path) => { return _.get(state, ['editableNodes', path]) },
//   isFieldEdited: (state) => ({ node, path }) => {
//     let partPath = [ node._type, 'nid-' + node.nid ]
//     return _.get(state, ['editedFields', [...partPath, ...path].join('.')])
//   },

//   currentSidebarPage: (state) => { return _.last(state.sidebarNavigation) },
//   backPageTitle: (state) => { return _.get(state.sidebarNavigation[state.sidebarNavigation.length - 2], 'title', 'Back') },

//   isAltPressed: state => state.altPressed,
//   isOriginalNodesDisplayed: state => state.originalNodesDisplayed,
//   haveActiveZoneSelected: state => _.some(_.values(state.activeZone), (val) => val),
//   showActiveZone: (state, getters) => getters.haveActiveZoneSelected || !_.isEmpty(state.defaultActiveFields),
//   activeFields: (state, getters) => getters.haveActiveZoneSelected ? state.lastActiveFields : state.defaultActiveFields,
//   activeTitle: (state, getters) => getters.haveActiveZoneSelected ? state.lastActiveTitle : state.defaultActiveTitle,
//   // currentContentType: state => state.currentContentType,
//   // getNodeByPath: (state, getters, rootState, rootGetters) => (path) => {
//   //   if (!getters.currentContentType || !rootGetters[getters.currentContentType + '/getByPath']) { return {} }
//   //   return rootGetters[getters.currentContentType + '/getByPath'](path)
//   // }
//   contentTypes: state => state.contentTypes,
//   newNodeId: state => state.newNodeId,

//   nodesList: state => state.nodesList,
//   getNodeByPath: (state) => (path) => { return _.get(state, ['nodesList', path], {}) },
//   getNodeByPathStatus: (state) => (path) => { return _.get(state, ['_nodesList', path]) },

//   routing: state => state.routing,
//   routingStatus: state => state._routing,
//   user: state => state.user,
//   userStatus: state => state._user
// }

// const actions = {
//   uploadEditableImage: (context, { data, options }) => {
//     return mapSetAction({
//       types,
//       context,
//       mutation: 'Y3TI_UPLOAD_IMAGE',
//       name: ['fileUpload', data.id],
//       status: ['_fileUpload', data.id],
//       call: api.uploadImage,
//       params: [data],
//       options: {
//         commitSuccess: ({ commit }, successMutation, params) => {
//           commit(successMutation, params)
//           commit(types.Y3TI_SAVE, { ...params, data: _.get(params.data, ['images', 0]) })
//         },
//         ...options
//       }
//     })
//   },

//   getMenu: (context, { options = {} } = {}) => {
//     return mapSetAction({
//       types,
//       context,
//       mutation: 'Y3TI_GET_MENU',
//       name: ['menu'],
//       status: ['_menu'],
//       call: api.getMenu,
//       params: [],
//       options: {
//         commitSuccess: ({ commit }, successMutation, params) => {
//           commit(successMutation, params)
//           commit(types.Y3TI_SAVE, params)
//         },
//         ...options
//       }
//     })
//   },

//   getContentTypes: (context, { options = {} }) => {
//     return mapSetAction({
//       types,
//       context,
//       mutation: 'Y3TI_GET_CONTENT_TYPES',
//       name: ['contentTypes'],
//       status: ['_contentTypes'],
//       call: api.getContentTypes,
//       params: [],
//       options: {
//         commitSuccess: ({ commit }, successMutation, params) => {
//           commit(successMutation, params)
//           commit(types.Y3TI_SAVE, params)
//         },
//         ...options
//       }
//     })
//   },

//   getFieldsConfig: (context, { data, options = {} }) => {
//     return mapSetAction({
//       types,
//       context,
//       mutation: 'Y3TI_GET_FIELDS_CONFIG',
//       name: ['fieldsConfig', data.contentType],
//       status: ['_fieldsConfig', data.contentType],
//       call: api.getFieldsConfig,
//       params: [data],
//       options: {
//         commitSuccess: ({ commit }, successMutation, params) => {
//           commit(successMutation, params)
//           commit(types.Y3TI_SAVE, params)
//         },
//         ...options
//       }
//     })
//   },

//   // setCurrentContentType: ({ commit }, { data: { contentType } }) => {
//   //   commit(types.Y3TI_SAVE, { name: [ 'currentContentType' ], data: contentType })
//   // },
//   setNodeItem: ({ commit }, { data: { path, node } }) => {
//     commit(types.Y3TI_SAVE, { name: [ 'nodesList', path ], data: node })
//   },

//   setEditableNode: ({ commit, dispatch, state }, { data: { path, node, related = false, forceRefresh = false } }) => {
//     let partPath = [ node._type, 'nid-' + node.nid ]
//     if (node._new) {
//       forceRefresh = true
//       commit(types.Y3TI_SAVE, { name: [ 'editableNodes', path, ...(related ? ['_related'] : []), node._type ], data: {} })
//     }
//     commit(types.Y3TI_SAVE, { name: [ 'editableNodes', path, ...(related ? ['_related'] : []), ...partPath ], data: node })
//     commit(types.Y3TI_SAVE, { name: [ 'originalNodes', ...partPath ], data: node })
//     if (!_.get(state, [ 'edited', ...partPath ]) || forceRefresh) {
//       commit(types.Y3TI_SAVE, { name: [ 'edited', ...partPath ], data: node })
//     }
//     // console.log('node', node)
//     return dispatch('getFieldsConfig', { data: { contentType: node._type }, options: { lazy: true } })
//   },

//   turnOnEditMode: ({ commit }) => {
//     commit(types.Y3TI_SAVE, { name: [ 'editMode' ], data: true })
//   },

//   turnOffEditMode: ({ commit }) => {
//     commit(types.Y3TI_SAVE, { name: [ 'editMode' ], data: false })
//   },

//   saveEditedNodeData: ({ dispatch, commit, state }, { data: { action, node } }) => {
//     if (action !== 'editNode' && action !== 'addNode') {
//       return false
//     }
//     if (_.isEmpty(node.title)) {
//       return false
//     }
//     let path = [ node._type, 'nid-' + node.nid ]
//     const editedData = _.get(state, [ 'edited', ...path ], {})
//     const originalData = _.get(state, [ 'originalNodes', ...path ], {})
//     const data = { nid: node.nid, data: editedData, originalPath: _.get(originalData, 'path.alias') }

//     // return dispatch(node._type + '/edit', { data }, { root: true }).then(({ data }) => {
//     return dispatch(action, { data }).then(({ data }) => {
//       const newPath = [ data._type, 'nid-' + data.nid ]
//       console.log('path', path)
//       console.log('newPath', newPath)
//       commit(types.Y3TI_SAVE, { name: [ 'edited', ...newPath ], data })
//       let editedFields = _.clone(state.editedFields)
//       editedFields = _.mapValues(editedFields, (val, key) => {
//         return _.startsWith(key, newPath.join('.')) ? false : val
//       })
//       if (!_.isEqual(newPath, path)) {
//         commit(types.Y3TI_SAVE, { name: [ 'edited', ...path ], data: {} })
//         editedFields = _.omitBy(editedFields, (val, key) => {
//           return _.startsWith(key, path.join('.'))
//         })
//       }
//       commit(types.Y3TI_SAVE, { name: [ 'editedFields' ], editedFields })
//       return dispatch('setEditableNode', { data: { path: _.get(data, 'path.alias'), node: data, forceRefresh: true } })
//     })
//   },

//   storeEditableFieldData: ({ commit, state }, { data: { node, path, input, raw } }) => {
//     if (state.originalNodesDisplayed) {
//       return false
//     }
//     // if (input === '<p>&nbsp;</p>') { return false }
//     let partPath = [ node._type, 'nid-' + node.nid ]

//     if (raw) {
//       commit(types.Y3TI_SAVE, { name: [ 'editedRawFields', _.concat(partPath, path, [raw]).join('.') ], data: _.clone(input) || '' })
//       if (rawFormatterFrom[raw]) {
//         input = rawFormatterFrom[raw](input)
//       }
//     } else {
//       commit(types.Y3TI_SAVE, { name: [ 'editedRawFields' ], data: _.omitBy(state.editedRawFields, (value, key) => { return _.startsWith(key, _.concat(partPath, path).join('.')) }) })
//     }

//     commit(types.Y3TI_SAVE, { name: [ 'edited', ...partPath, ...path ], data: input })
//     // console.log('looo', _.concat(partPath, path).join('.'))
//     // console.log('1: ', input)
//     // console.log('2: ', _.get(state, [ 'originalNodes', ...partPath, ...path ]))
//     // console.log('=: ', !_.isEqual(input, _.get(state, [ 'originalNodes', ...partPath, ...path ])))
//     commit(types.Y3TI_SAVE, { name: [ 'editedFields', _.concat(partPath, path).join('.') ], data: !_.isEqual(input, _.get(state, [ 'originalNodes', ...partPath, ...path ])) })
//   },

//   setSidebarNav: ({ commit, state }, { data: { nav } }) => {
//     if (!nav) {
//       commit(types.Y3TI_SAVE, { name: [ 'sidebarNavigation' ], data: [] })
//     } else {
//       const navigation = JSON.parse(window.atob(nav))
//       if (_.isArray(navigation)) {
//         commit(types.Y3TI_SAVE, { name: [ 'sidebarNavigation' ], data: navigation })
//       }
//     }
//   },

//   pushSidebarPage: ({ commit, state }, { data: { page, action = 'replace' } }) => {
//     let nav = _.clone(_.get(state, [ 'sidebarNavigation' ], []))
//     nav.push({ ...page, name: router.currentRoute.name, params: router.currentRoute.params })
//     // console.log('ah?')
//     // console.log('router', router)
//     // console.log('window.btoa(JSON.stringify(nav))', window.btoa(JSON.stringify(nav)))
//     // console.log('window.btoa(nav)', window.btoa(nav))
//     // console.log('window.atob(nav)', JSON.parse(window.atob(window.btoa(JSON.stringify(nav)))))
//     // console.log('--hash', window.btoa(JSON.stringify(nav)))
//     router[action]({ ...router.currentRoute, hash: '#' + window.btoa(JSON.stringify(nav)) })
//     commit(types.Y3TI_SAVE, { name: [ 'sidebarNavigation' ], data: nav })
//   },
//   // replaceSidebarPage: ({ commit, state }, { data: { page } }) => {
//   //   let nav = _.clone(_.get(state, [ 'sidebarNavigation' ], []))
//   //   nav[nav.length - 1] = page
//   //   router.replace({ ...router.currentRoute, hash: '#' + window.btoa(JSON.stringify(nav)) })
//   //   commit(types.Y3TI_SAVE, { name: [ 'sidebarNavigation' ], data: nav })
//   // },

//   backSidebarPage: ({ commit, state }) => {
//     let nav = _.clone(_.get(state, [ 'sidebarNavigation' ], []))
//     _.pullAt(nav, [nav.length - 1])
//     // console.log('router.currentRoute', router.currentRoute)
//     let pushWith = { hash: '' }
//     if (!_.isEmpty(nav)) {
//       let currentNav = nav[nav.length - 1]
//       // currentNav.params = currentNav.props
//       pushWith.hash = '#' + window.btoa(JSON.stringify(nav))
//       pushWith = { ...pushWith, ...currentNav }
//     } else {
//       pushWith.path = '/'
//     }
//     router.push(pushWith)
//     commit(types.Y3TI_SAVE, { name: [ 'sidebarNavigation' ], data: nav })
//   },

//   pushEditableFieldData: ({ commit, state }, { data: { node, path, config } }) => {
//     let partPath = [ node._type, 'nid-' + node.nid ]
//     let dataArray = _.clone(_.get(state, [ 'edited', ...partPath, ...path ], []))
//     if (!_.isArray(dataArray)) {
//       dataArray = []
//     }
//     const input = config.cardinality === -1 ? config.default[0] : (config.cardinality === 1 ? config.default : config.default[dataArray.length])
//     // console.log('input', input)
//     dataArray.push(input)
//     commit(types.Y3TI_SAVE, { name: [ 'edited', ...partPath, ...path ], data: dataArray })
//     commit(types.Y3TI_SAVE, { name: [ 'editedFields', _.concat(partPath, path, [dataArray.length - 1]).join('.') ], data: !_.isEqual(_.last(dataArray), _.get(state, [ 'originalNodes', ...partPath, ...path, dataArray.length - 1 ])) })
//   },

//   pullEditableFieldData: ({ commit, state }, { data: { node, path, config } }) => {
//     const index = _.last(path)
//     const parentPath = path.slice(0, -1)
//     let partPath = [ node._type, 'nid-' + node.nid ]
//     let dataArray = _.clone(_.get(state, [ 'edited', ...partPath, ...parentPath ], []))
//     _.pullAt(dataArray, [index])
//     // console.log('dataArray', dataArray)
//     if (_.isEmpty(dataArray)) {
//       dataArray = null
//     }
//     commit(types.Y3TI_SAVE, { name: [ 'edited', ...partPath, ...parentPath ], data: dataArray })
//     commit(types.Y3TI_SAVE, { name: [ 'editedRawFields' ], data: _.omitBy(state.editedRawFields, (value, key) => { return _.startsWith(key, _.concat(partPath, parentPath).join('.')) }) })
//     commit(types.Y3TI_SAVE, { name: [ 'editedFields', _.concat(partPath, parentPath).join('.') ], data: !_.isEqual(dataArray, _.get(state, [ 'originalNodes', ...partPath, ...parentPath ])) })
//   },

//   // applyDefaultEditableFieldData: ({ dispatch, state }, { data: { node, path } }) => {
//   //   const input = _.get(state, [ 'originalNodes', ...path ])
//   //   dispatch('storeEditableFieldData', { data: { node, path, input } })
//   // },

//   changeEditableParagraphType: ({ dispatch, state }, { data: { node, path, config, type } }) => {
//     let partPath = [ node._type, 'nid-' + node.nid ]
//     // const origin = _.get(state, [ 'originalNodes', ...partPath, ...path ])
//     const existing = _.get(state, [ 'edited', ...partPath, ...path ])
//     const index = _.last(path)
//     let defaultValues = _.get(config, [ 'default_ref', type ], {})

//     if (_.isInteger(index) && _.isArray(defaultValues) && config.cardinality !== 1) {
//       defaultValues = config.cardinality === -1 ? defaultValues[0] : defaultValues[index]
//     }

//     let input = { _type: type }
//     // if (origin && origin._type === input._type) {
//     //   // input = _.assign({}, origin, existing, input)
//     //   input = _.assignWith({}, origin, existing, input, (a, b) => b === null ? a : undefined)
//     //   input = _.pick(input, _.keys(origin))
//     // } else {
//     // input = _.assign({}, defaultValues, existing, input)
//     input = _.assignWith({}, defaultValues, existing, input, (a, b) => b === null ? a : undefined)
//     input = _.pick(input, _.keys(defaultValues))
//     // }
//     return dispatch('storeEditableFieldData', { data: { node, path, input } })
//   },

//   // updateMultipleEditableFieldsData: ({ dispatch, state }, { data: { node, path, config, keep = ['_type'] } }) => {
//   //   let partPath = [ node._type, 'nid-' + node.nid ]
//   //   const defaultValues = _.get(state, [ 'originalNodes', ...partPath, ...path ])
//   //   const existing = _.get(state, [ 'edited', ...partPath, ...path ])
//   //   // const coercedExisting = _.pick(existing, _.concat(_.keys(config), keep))
//   //   console.log('existing', existing)
//   //   console.log('config', config)
//   //   const coercedExisting = _.pickBy(existing, (val, key) => {
//   //     return keep.includes(key) || !_.get(config, [key, 'read_only'], true)// && !_.get(config, [key, 'paragraph'], false)
//   //   })
//   //   // console.log(defaultValues)
//   //   console.log('coercedExisting', coercedExisting)
//   //   const input = _.isEqual(_.pick(defaultValues, keep), _.pick(coercedExisting, keep)) ? _.assign({}, defaultValues, coercedExisting) : coercedExisting
//   //   dispatch('storeEditableFieldData', { data: { node, path, input } })
//   // },

//   setEditableActiveZone: ({ commit }, { data: { id, register, title, isDefault } }) => {
//     const fields = _.map(register, (item) => {
//       return {
//         type: item.node ? item.node._type : null,
//         key: item.node ? 'nid-' + item.node.nid : null,
//         path: item.path
//       }
//     })

//     if (isDefault) {
//       commit(types.Y3TI_SAVE, { name: [ 'defaultActiveTitle' ], data: title })
//       commit(types.Y3TI_SAVE, { name: [ 'defaultActiveFields' ], data: fields })
//     } else {
//       commit(types.Y3TI_SAVE, { name: [ 'lastActiveTitle' ], data: title })
//       commit(types.Y3TI_SAVE, { name: [ 'lastActiveFields' ], data: fields })
//       commit(types.Y3TI_SAVE, { name: [ 'activeZone' ], data: { [id]: true } })
//       // commit(types.Y3TI_SAVE, { name: [ 'activeZone', id ], data: true })
//     }
//   },

//   unsetEditableActiveZone: ({ commit }, { data: { id, isDefault } }) => {
//     if (isDefault) {
//       commit(types.Y3TI_SAVE, { name: [ 'defaultActiveTitle' ], data: null })
//       commit(types.Y3TI_SAVE, { name: [ 'defaultActiveFields' ], data: [] })
//     } else {
//       // commit(types.Y3TI_SAVE, { name: [ 'lastActiveTitle' ], data: null })
//       // commit(types.Y3TI_SAVE, { name: [ 'lastActiveFields' ], data: [] })
//       if (id === '*') {
//         commit(types.Y3TI_SAVE, { name: [ 'activeZone' ], data: {} })
//       } else {
//         commit(types.Y3TI_SAVE, { name: [ 'activeZone', id ], data: false })
//       }
//     }
//     // commit(types.Y3TI_SAVE, { name: [ 'activeFields' ], data: [] })
//     // commit(types.Y3TI_SAVE, { name: [ 'activeTitle' ], data: null })
//   },

//   setAltPressed: ({ commit }) => {
//     commit(types.Y3TI_SAVE, { name: [ 'altPressed' ], data: true })
//   },

//   unsetAltPressed: ({ commit }) => {
//     commit(types.Y3TI_SAVE, { name: [ 'altPressed' ], data: false })
//   },

//   setOriginalNodesDisplay: ({ commit }, value) => {
//     commit(types.Y3TI_SAVE, { name: [ 'originalNodesDisplayed' ], data: value })
//   },

//   revertAllToOriginalNodes: ({ commit, state }) => {
//     // console.log('originalNodes', _.merge({}, _.get(state, [ 'originalNodes' ])))
//     // console.log('edited', _.merge({}, _.get(state, [ 'edited' ])))
//     commit(types.Y3TI_SAVE, { name: [ 'editedFields' ], data: {} })
//     commit(types.Y3TI_SAVE, { name: [ 'editedRawFields' ], data: {} })
//     commit(types.Y3TI_SAVE, { name: [ 'edited' ], data: _.get(state, [ 'originalNodes' ]) })
//     commit(types.Y3TI_SAVE, { name: [ 'originalNodesDisplayed' ], data: false })
//     // state.edited = _.clone(state.originalNodes)
//   },

//   setNewNodeId: ({ commit }) => {
//     commit(types.Y3TI_SAVE, { name: [ 'newNodeId' ], data: Date.now() })
//   },

//   getOneNode: (context, { data, options = {} } = {}) => {
//     return mapSetAction({
//       types,
//       context,
//       mutation: 'Y3TI_GET_ONE_NODE',
//       name: ['nodesList', data.path],
//       status: ['_nodesList', data.path],
//       call: api.getOneNode,
//       params: [data],
//       options: {
//         commitSuccess: ({ commit }, successMutation, params) => {
//           commit(successMutation, params)
//           commit(types.Y3TI_SAVE, params)
//         },
//         ...options
//       }
//     })
//   },

//   editNode: (context, { data, options = {} } = {}) => {
//     const nid = _.get(data, 'data.nid')
//     return mapSetAction({
//       types,
//       context,
//       mutation: 'Y3TI_EDIT_NODE',
//       name: false,
//       status: ['_nodesEdit', 'nid-' + nid],
//       call: api.editNode,
//       params: [data],
//       options: {
//         commitSuccess: ({ commit }, successMutation, params) => {
//           const newPath = _.get(params, 'data.path.alias')
//           commit(successMutation, params)
//           commit(types.Y3TI_SAVE, { ...params, name: [ 'nodesList', newPath ] })
//           if (data.originalPath !== newPath) {
//             // To re-grab and parse router. /!\ Should be done async, because right now, other nodes changes will be lost.
//             window.location.reload()
//           }
//         },
//         ...options
//       }
//     })
//   },

//   addNode: (context, { data, options = {} } = {}) => {
//     let nodesList = _.get(context.state, [ 'nodesList' ])

//     // const path = _.get(data, 'data.path.alias')
//     const nid = _.get(data, 'data.nid')
//     return mapSetAction({
//       types,
//       context,
//       mutation: 'Y3TI_ADD_NODE',
//       name: false,
//       status: ['_nodesAdd', 'nid-' + nid],
//       call: api.addNode,
//       params: [data],
//       options: {
//         commitSuccess: ({ commit }, successMutation, params) => {
//           const newPath = _.get(params, 'data.path.alias')
//           commit(successMutation, params)

//           nodesList = _.omitBy(nodesList, (fields, path) => {
//             return fields.nid === nid
//           })
//           commit(types.Y3TI_SAVE, { name: [ 'nodesList' ], data: nodesList })
//           commit(types.Y3TI_SAVE, { ...params, name: [ 'nodesList', newPath ] })
//           // if (data.originalPath !== path) {
//           //   // To re-grab and parse router. /!\ Should be done async, because right now, other nodes changes will be lost.
//           //   window.location.reload()
//           // }
//           console.log(params)
//           if (newPath) {
//             window.location = newPath
//           }
//         },
//         ...options
//       }
//     })
//   },

//   getRouting: (context, { options }) => {
//     return mapSetAction({
//       types,
//       context,
//       mutation: 'Y3TI_GET_ROUTING',
//       name: ['routing'],
//       status: ['_routing'],
//       call: api.getRouting,
//       params: [],
//       options: {
//         commitSuccess: ({ commit }, successMutation, params) => {
//           commit(successMutation, params)
//           commit(types.Y3TI_SAVE, params)
//         },
//         ...options
//       }
//     })
//   },

//   getUser: (context, { options }) => {
//     return mapSetAction({
//       types,
//       context,
//       mutation: 'Y3TI_GET_USER',
//       name: ['user'],
//       status: ['_user'],
//       call: api.getUser,
//       params: [],
//       options: {
//         commitSuccess: ({ commit }, successMutation, params) => {
//           commit(successMutation, params)
//           commit(types.Y3TI_SAVE, params)
//         },
//         ...options
//       }
//     })
//   }
// }

// const mutations = {
//   ...mapSetMutations({ types, mutation: 'Y3TI_GET_CONTENT_TYPES' }),
//   ...mapSetMutations({ types, mutation: 'Y3TI_UPLOAD_IMAGE' }),
//   ...mapSetMutations({ types, mutation: 'Y3TI_GET_MENU' }),
//   ...mapSetMutations({ types, mutation: 'Y3TI_GET_FIELDS_CONFIG' }),
//   ...mapSetMutations({ types, mutation: 'Y3TI_GET_ONE_NODE' }),
//   ...mapSetMutations({ types, mutation: 'Y3TI_EDIT_NODE' }),
//   ...mapSetMutations({ types, mutation: 'Y3TI_ADD_NODE' }),
//   ...mapSetMutations({ types, mutation: 'Y3TI_GET_ROUTING' }),
//   ...mapSetMutations({ types, mutation: 'Y3TI_GET_USER' }),

//   ...mutationSave(types.Y3TI_SAVE),
//   ...mutationSetStatus(types.Y3TI_RESET_STATUS, null)
// }

// export default {
//   namespaced: true,
//   state,
//   getters,
//   actions,
//   mutations
// }
