// import _ from 'lodash'

// import BasicLayout from '@/components/Layouts/BasicLayout'
// import CustomLayout from '@/components/Layouts/CustomLayout'
// import DefaultLayout from '@/components/Layouts/DefaultLayout'

// import MenuBlock from '@/components/Blocks/MenuBlock'

// import DefaultComponent from '@/components/Default'
// import PageComponent from '@/components/Page'

// import Page from '@/components/Page'
// import Test from '@/components/Test'
// import NewNode from '@/components/NewNode'

// import router from '@/router'

// const defaultLayoutItem = {
//   path: '',
//   component: DefaultLayout,
//   children: [{
//     path: '',
//     components: {
//       'menu': MenuBlock,
//       'main-content': { template: '<router-view/>' }
//     },
//     children: []
//   }]
// }

// const layoutsMapping = {
//   default: defaultLayoutItem,

//   custom: {
//     path: '',
//     component: CustomLayout,
//     children: [{
//       path: '',
//       components: {
//         'menu': MenuBlock,
//         'main-content': { template: '<router-view/>' }
//       },
//       children: []
//     }]
//   }
// }

// const defaultComponentItem = DefaultComponent

// const componentsMapping = {
//   custom: CustomLayout,
//   default: defaultComponentItem
// }

// const layoutsMapping = {
//   default: {
//     path: '',
//     component: DefaultLayout,
//     children: [{
//       path: '',
//       components: {
//         'menu': MenuBlock,
//         'main-content': { template: '<router-view/>' }
//       },
//       children: []
//     }]
//   },

//   custom: {
//     path: '',
//     component: CustomLayout,
//     children: [{
//       path: '',
//       components: {
//         'menu': MenuBlock,
//         'main-content': { template: '<router-view/>' }
//       },
//       children: []
//     }]
//   }
// }

// const componentsMapping = {
//   default: DefaultComponent,
//   page: PageComponent
// }

// export const layouts = layoutsMapping
// export const components = componentsMapping

// export const pushRoutes = (rawRoutes) => {
//   let routes = []
//   let parsedRoutes = {}
//   _.each(rawRoutes, (route, path) => {
//     const parsedRoutesPath = [ route.layout ]
//     let routesPaths = _.get(parsedRoutes, parsedRoutesPath, [])
//     routesPaths.push({
//       path,
//       component: route.component
//     })
//     _.set(parsedRoutes, parsedRoutesPath, routesPaths)
//   })

//   const routesLayoutPath = ['children', 0, 'children']
//   _.each(parsedRoutes, (layoutRoutes, layout) => {
//     let routesLayout = layoutsMapping[layout] || layoutsMapping.default

//     let routesLayoutChildren = _.get(routesLayout, routesLayoutPath, [])
//     _.each(layoutRoutes, ({ path, component }) => {
//       routesLayoutChildren.push({
//         path,
//         // name: 'NewPresetNode',
//         props: (route) => ({
//           path: route.path,
//           ...route.params
//         }),
//         component: componentsMapping[component] || componentsMapping.default
//       })
//     })

//     // const layoutComponentName = _.get(routesLayout, 'component.name')
//     routesLayoutChildren.push({
//       path: '/:contentType/new',
//       name: 'NewNode-' + layout,
//       props: (route) => ({
//         path: route.path,
//         ...route.params
//       }),
//       component: NewNodeComponent
//     },
//     {
//       path: '/:contentType/:preset/new',
//       name: 'NewPresetNode-' + layout,
//       props: (route) => ({
//         path: route.path,
//         ...route.params
//       }),
//       component: NewNodeComponent
//     })

//     _.set(routesLayout, routesLayoutPath, routesLayoutChildren)
//     routes.push(routesLayout)
//   })

//   console.log('--routes', routes)
//   router.addRoutes(routes)
// }
