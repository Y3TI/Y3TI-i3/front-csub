import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'

// import store from '../store'

// import _ from 'lodash'

// import { cookie } from '@/utils/tools'

// import BasicLayout from '@/components/Layouts/BasicLayout'
// import MenuBlock from '@/components/Blocks/MenuBlock'

// import Page from '@/components/Page'
// import Test from '@/components/Test'
// import NewNode from '@/components/NewNode'

Vue.use(Router)
Vue.use(Meta)

let router = new Router({
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    { path: '*', name: '404', component: { template: '<div>404</div>' } }
    // {
    //   path: '',
    //   component: BasicLayout,
    //   children: [{
    //     path: '',
    //     components: {
    //       'menu': MenuBlock,
    //       'main-content': { template: '<router-view/>' }
    //     },
    //     children: [
    //       {
    //         path: '/',
    //         name: 'Home',
    //         props: (route) => ({ path: route.path }),
    //         component: Page
    //       },
    //       {
    //         path: '/welcom-yfgnvbe',
    //         name: 'Page',
    //         props: (route) => ({ path: route.path }),
    //         component: Page
    //       },
    //       {
    //         path: '/bhbhbhbhbhbh',
    //         name: 'Test',
    //         props: (route) => ({ path: route.path }),
    //         component: Test
    //       },
    //       {
    //         path: '/:contentType/new',
    //         name: 'NewNode',
    //         props: (route) => ({
    //           path: route.path,
    //           contentType: route.params.contentType,
    //           keepSidebar: route.params.keepSidebar
    //         }),
    //         component: NewNode
    //       },
    //       {
    //         path: '/:contentType/:preset/new',
    //         name: 'NewPresetNode',
    //         props: (route) => ({
    //           path: route.path,
    //           contentType: route.params.contentType,
    //           preset: route.params.preset,
    //           keepSidebar: route.params.keepSidebar
    //         }),
    //         component: NewNode
    //       }
    //     ]
    //   }]
    // }
  ]
})

// router.beforeEach((to, from, next) => {
//   store.dispatch('Y3TI/setSidebarNav', { data: { nav: (_.isEmpty(to.hash) ? false : to.hash.substring(1)) } })

//   const routingDefinition = _.get(store, ['getters', 'Y3TI/routing', 'definition'], {})
//   _.each(['NewNode-', 'NewPresetNode-'], (dynamicLayoutRoutePattern) => {
//     if (_.startsWith(to.name, dynamicLayoutRoutePattern)) {
//       const route = routingDefinition[to.params.contentType]
//       if (route) {
//         const routeName = dynamicLayoutRoutePattern + route.layout
//         if (routeName !== to.name) {
//           next({ name: routeName, params: to.params })
//         }
//       }
//     }
//   })

//   // console.log('store', _.get(store, ['getters', 'Y3TI/routing', 'definition']))
//   // console.log('to', to)

//   // console.log('from', from)
//   // console.log('to', to)
//   // console.log('routingDefinition', routingDefinition)
//   // const route = routingDefinition[to.params.contentType]
//   // if (route && _.startsWith(to.name, 'NewNode-')) {
//   //   const routeName = 'NewNode-' + route.layout
//   //   console.log('laaaa')
//   //   // console.log('--this.$route.name', this.$route.name)
//   //   if (routeName !== to.name) {
//   //     console.log('loooooo')
//   //     // console.log('--routeName', routeName)
//   //     next({ name: routeName, params: to.params })
//   //   }
//   // }

//   // if (window.showUnpublished === undefined) {
//   //   window.showUnpublished = to.query.unpublished === 'true' || cookie.get('showUnpublished') === 'true'
//   // }

//   // if (window.editToolbar && window.showUnpublished && from.query.unpublished === 'true' && !to.query.unpublished) {
//   //   next({ params: to.params, path: to.path, query: { unpublished: 'true' } })
//   // } else {
//   //   next()
//   // }
//   next()
// })

// router.afterEach((to, from) => {
//   store.dispatch('Y3TI/unsetEditableActiveZone', { data: { isDefault: true } })
// })

export default router
