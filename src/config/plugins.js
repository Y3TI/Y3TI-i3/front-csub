// /!\ Don't trust hot reload here !
const path = require('path')
const withSources = true

function resolve (dir) {
  return path.join(__dirname, '..', '..', dir)
}

let sourcesVariables = {
  ALIASES: {
    'ß': resolve('node_modules/y3ti-i3/src'),
    '©': resolve('node_modules/y3ti-r3/src'),
    'Y3TII3_CSS': resolve('node_modules/y3ti-i3/src/assets/style/_s4squash.scss')
  },
  INCLUDE_SRC_FROM_ALIASES: true,
  WITH_SOURCES: true
  // Y3TII3_PATH: 'node_modules/y3ti-i3/src',
  // Y3TIR3_PATH: 'node_modules/y3ti-r3/src',
  // Y3TII3_PATH: '/Users/squark/pSync/Y3TI/_clients/CSUB/themes/csub/y3ti-i3/src',
  // Y3TIR3_PATH: '/Users/squark/pSync/Y3TI/_clients/CSUB/themes/csub/y3ti-r3/src',
}
// developmentVariables.Y3TII3_CSS = developmentVariables.Y3TII3_PATH + '/assets/style/_s4squash.scss'

let compiledVariables = {
  ALIASES: {
    // 'ß': 'y3ti-i3',
    // '©': 'y3ti-r3',
    'Y3TII3_CSS': 'y3ti-i3/static/css/y3ti-i3.css'
  },
  WITH_SOURCES: false
}
// productionVariables.Y3TII3_CSS = productionVariables.Y3TII3_PATH + '/static/css/app.css'

module.exports = withSources ? sourcesVariables : compiledVariables
