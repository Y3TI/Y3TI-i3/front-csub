const _ = require('lodash')

let environment = process.env.NODE_ENV

let config = _.merge({}, require('./plugins'), require('./dev').default)
if (environment === 'testing') {
  _.merge(config, require('./prod').default, require('./test').default)
  console.info(`-- Testing environment`)
} else if (environment === 'production') {
  _.merge(config, require('./prod').default)
} else if (environment === 'development') {
  console.info(`-- Development environment`)
} else {
  console.warn('No correct environment provided.')
  console.info(`-- Development environment`)
  environment = 'development'
}

_.merge(config, { ENV: environment })

module.exports = config
