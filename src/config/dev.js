// ! \\ If not overrided in prod or test config, a dev settings will be set for all environments!
// import ReuseMediaCheckbox from '@/components/Editable-Custom/ReuseMediaCheckbox'
//     ItemsForm: () => import('@/components/Editable-Custom/ReuseMediaCheckbox')

import CustomLayout from '@/components/Layouts/CustomLayout'
import DefaultLayout from '@/components/Layouts/DefaultLayout'

import MenuBlock from '@/components/Blocks/MenuBlock'

import DefaultComponent from '@/components/Default'
import PageComponent from '@/components/Page'
import TestComponent from '@/components/Test'

import PageAhPreset from '@/node-presets/page/ah'
import PageCustomPreset from '@/node-presets/page/custom'

// const CustomLayout = require('@/components/Layouts/CustomLayout')
// const DefaultLayout = require('@/components/Layouts/DefaultLayout')

// const MenuBlock = require('@/components/Blocks/MenuBlock')

// const DefaultComponent = require('@/components/Default')
// const PageComponent = require('@/components/Page')
// const TestComponent = require('@/components/Test')

const ghostZones = (node) => {
  return {
    page: { title: 'Ooooh', register: [{ node, path: ['field_links', 0] }] }
  }
}

export default {
  TITLE: 'CSU Bakersfield',
  // API_URL: window.location.origin + '/api'
  // API_URL: 'http://inline.csub.y3ti/api',
  API_URL: 'http://csub.wip/api',
  QUERY_MERGE: { timestamp: Date.now() },

  EDITABLE_FIELDS_GROUP_CONFIG (props) {
    let groupOptions = {
      // beforeAll: [ 'reuse-media-checkbox' ],
      // afterAll: [ 'reuse-media-checkbox' ]
    }
    // if (config.id === 'accordion') {
    //   groupOptions.hideAll = true
    // }

    // console.log('- config', config.id)
    return groupOptions
  },
  EDITABLE_FIELD_CONFIG (props) {
    // console.log(ReuseMediaCheckbox)
    let fieldOptions = {
      // beforeField: {},
      // afterField: {}
    }
    // if (config.reference === 'media' && fieldConfig.type === 'image') {
    //   fieldOptions = {
    //     beforeField: [ 'reuse-media-checkbox' ]
    //     // hideField: true
    //     // afterField: [ 'reuse-media-checkbox' ]
    //   }
    // }

    // if (fieldConfig.id === 'field_pg_rows') {
    //   fieldOptions.hideField = true
    // }
    // console.log('-- config', config.id === 'field_pg_rows')
    // console.log('-- fieldConfig', fieldConfig.path)
    return fieldOptions
  },
  EDITABLE_WIDGETS_MAPPING (props) {
    return false
  },
  EDITABLE_SETTINGS_FIELD_CONFIG (props) {
    let fieldOptions = {
      // beforeField: {},
      // afterField: {}
    }

    // if (fieldConfig.type === 'checkbox') {
    //   fieldOptions = {
    //     beforeField: [ 'reuse-media-checkbox' ]
    //     // hideField: true
    //     // afterField: [ 'reuse-media-checkbox' ]
    //   }
    // }

    return fieldOptions
  },
  EDITABLE_SETTINGS_FIELDS_MAPPING (props) {
    return false
  },
  // EDITABLE_NODE_PRESETS (node) {
  //   const ghostZonesList = ghostZones(node)
  //   return {
  //     page: {
  //       custom: {
  //         data: require('@/node-presets/page/custom'),
  //         ghostZone: ghostZonesList.page
  //       }
  //     }
  //   }
  // },
  EDITABLE_NODE_PRESETS: {
    page: {
      custom: { name: 'Custom', data: PageCustomPreset },
      ah: { name: 'Ah', data: PageAhPreset }
    }
  },
  EDITABLE_GHOST_ZONES (node) {
    return ghostZones(node)
  },

  EDITABLE_NEW_NODE_COMPONENTS: {
    page: PageComponent,
    test: TestComponent
  },

  EDITABLE_CUSTOM_WIDGETS: {
    // 'reuse-media-checkbox': () => import('@/components/Editable-Custom/ReuseMediaCheckbox'),
  },

  ROUTING: {
    layoutsMapping: {
      default: {
        path: '',
        component: DefaultLayout,
        children: [{
          path: '',
          components: {
            'menu': MenuBlock,
            'main-content': { template: '<router-view/>' }
          },
          children: []
        }]
      },
      custom: {
        path: '',
        component: CustomLayout,
        children: [{
          path: '',
          components: {
            'menu': MenuBlock,
            'main-content': { template: '<router-view/>' }
          },
          children: []
        }]
      }
    },

    componentsMapping: {
      default: DefaultComponent,
      page: PageComponent
    }
  }
}
