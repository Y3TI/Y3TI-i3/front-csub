import _ from 'lodash'

import { EDITABLE_GHOST_ZONES } from '@/components/Editable/config'

import { mapGetters, mapActions } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      getNodeByPath: 'Y3TI/getNodeByPath',
      editMode: 'Y3TI/editMode',
      getEditedNodes: 'Y3TI/getEditedNodes',
      getFieldConfig: 'Y3TI/getFieldConfig'
    }),

    originalNode () {
      return this.getNodeByPath(this.path)
    },

    currentNode () {
      let node = this.originalNode
      if (this.editMode && !_.isEmpty(node)) {
        const editedNode = this.getEditedNodes([ node._type, 'nid-' + node.nid ])
        if (editedNode) {
          node = editedNode
        }
      }
      return node
    },

    nodeLoaded () {
      let check = !_.isEmpty(this.currentNode)
      return this.editMode ? check && !_.isEmpty(this.getFieldConfig([ this.currentNode._type ])) : check
    }
  },

  methods: {
    ...mapActions({
      getOneNodeCall: 'Y3TI/getOneNode',
      setEditableNodeCall: 'Y3TI/setEditableNode',
      setEditableActiveZone: 'Y3TI/setEditableActiveZone'
    }),

    async getCurrentNode (ghostZone) {
      Promise.resolve().then(() => {
        if (this.isNew) {
          return true
        } else {
          return this.getOneNodeCall({ data: { path: this.path }, options: { lazy: false } }).catch((err) => {
            console.log(err)
          })
        }
      }).then(() => {
        if (this.editMode) {
          this.setEditableNode(this.originalNode)
          this.handleGhostZone(ghostZone)
        }
      })
      // await this.getOneNodeCall({ data: { path: this.path }, options: { lazy: false } }).catch((err) => {
      //   console.log(err)
      // }).then(() => {
      //   if (this.editMode) {
      //     this.setEditableNode(this.originalNode)
      //     this.handleGhostZone(ghostZone)
      //   }
      // })
    },

    handleGhostZone (ghostZone) {
      const type = _.get(this.currentNode, '_type')
      if (!ghostZone && type) {
        const ghostZones = EDITABLE_GHOST_ZONES(this.currentNode)
        if (ghostZones) {
          ghostZone = ghostZones[type]
        }
        // const defaultGhostZone = EDITABLE_GHOST_ZONE(node)[type]
        // if (defaultGhostZone) {
        //   ghostZone = {
        //     title: defaultGhostZone.title,
        //     register: _.map(defaultGhostZone.register, (item) => {
        //       return {
        //         ...item,
        //         node: this.currentNode
        //       }
        //     })
        //   }
        // }
      }

      if (ghostZone) {
        this.setEditableActiveZone({ data: { isDefault: true, ...ghostZone } })
      }
    },

    async setEditableNode (node, related) {
      await this.setEditableNodeCall({ data: { path: this.path, node, related } }).catch((err) => {
        console.log(err)
      })
    }
  }
}
